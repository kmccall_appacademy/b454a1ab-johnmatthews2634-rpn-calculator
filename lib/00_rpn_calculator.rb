class RPNCalculator
  # TODO:
  def initialize
    @array = []
  end

  def push(num)
    @array << num
  end

  def plus
    raise_error
    result = @array.pop + @array.pop
    @array << result

  end

  def value
    @array[-1]
  end

  def minus
    raise_error
    result = @array.pop * -1 + @array.pop
    @array << result
  end

  def divide
    raise_error
    result = @array[-2].to_f / @array[-1].to_f
    @array.pop
    @array.pop
    @array << result
  end

  def times
    raise_error
    result = @array.pop * @array.pop
    @array << result
  end

  def raise_error
    if @array.length < 2
      raise "calculator is empty"
    end

  end

  def tokens(string)
    new_array = string.split

    new_array.each do |ele|
      if ele == "+"
        @array << :+
      elsif ele == "/"
        @array << :/
      elsif ele == "-"
        @array << :-
      elsif ele == "*"
        @array << :*
      else
        @array << ele.to_i
      end
    end
    @array
  end

  def evaluate(string)
    eval_array = tokens(string)
    last_int = 0
    first_operand = 0
    eval_array.each_with_index do |ele, idx|
      if ele.class == Fixnum
        next
      else
        last_int = idx - 1
        first_operand = idx
        break
      end
    end
    nums_array = eval_array[0..last_int]
    operands_array = eval_array[first_operand..-1]
    @array = nums_array

    operands_array.each do |operand|
      if operand == :+
        nums_array.plus
      elsif operand == :-
        nums_array.minus
      elsif operand == :*
        nums_array.times
      elsif operand == :/
        nums_array.divide
      end
    end
    nums_array.calculator.value


  end









end
